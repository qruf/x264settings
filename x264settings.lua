#! /usr/bin/env lua

local execf = function(str, ...)
    local fd = io.popen(string.format(str, ...))
    local out = fd:read("*a")
    assert(fd:close())
    return out
end

local presets = { "ultrafast", "superfast", "veryfast", "faster", "fast", "medium", "slow", "slower", "veryslow", "placebo" }
local settings = {}

for _, preset in pairs(presets) do
    local out = {}
    local buf = execf("echo abcdef | x264 - --input-res 2x2 --preset %s -o - 2>/dev/null", preset)
    local sei = buf:match("(x264.-)\0")
    for k, v in sei:gmatch("(%S+)=(%S+)") do
        out[k] = v
    end
    settings[preset] = out
end

local diff, same = {}, {}
for setting, val in pairs(settings.medium) do
    for _, preset in pairs(presets) do
        if not settings[preset][setting] then
            goto skip
        end
        if settings[preset][setting] ~= val then
            diff[setting] = true
            diff[#diff+1] = setting
            goto done
        end
        ::skip::
    end
    if not setting:match("threads") then
        same[#same+1] = setting
    end
    ::done::
end

table.sort(diff)
table.sort(same)

io.output("settings.md")

io.write("## x264 settings that differ between presets\n")
io.write("| | ")
for _, preset in ipairs(presets) do 
    io.write("**", preset, "** | ")
end
io.write("\n", string.rep("| --- ", #presets+1), "|\n")

for _, setting in ipairs(diff) do
    io.write("| **", setting, "** | ")
    for _, preset in ipairs(presets) do
        local out = settings[preset][setting] or "-"
        io.write(out == settings.medium[setting] and out or "***" .. out .. "***", " | ")
    end
    io.write("\n")
end

io.write("\n## x264 settings that are the same for all presets\n")
for _, setting in ipairs(same) do
    io.write("- **", setting, ":** ", settings.placebo[setting], "\n")
end

