## x264settings

A script that generates a Markdown table of x264 encoding settings, sorted by preset. Requires x264 and Lua 5.2.

A reasonably up-to-date list of settings can be found [here](https://gitlab.com/qruf/x264settings/blob/master/settings.md).

Idea shamelessly stolen from [http://dev.beandog.org/x264_preset_reference.html].

